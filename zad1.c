#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#include "funkcje.h"

struct Dane
{
    double X;
    double Y;
    double RHO;
};

int main()
{
    FILE *plik ;
    if((plik=fopen("P0001_attr.txt","a+"))==NULL)
    {
         printf("blad otwarcia pliku\n");
    }

   struct Dane dane[55];
   

   char znak="";
   char liczba[10]="";
   int n=0;
   int kolumna=0,wiersz=0;

   while(1)
    {
        if(wiersz==50)
        {
            printf("Dane sa juz nadpisane.");
            return 0;
        }
        znak = fgetc(plik); //zapisuję jeden znak z pliku
        if(znak==9 || znak==EOF)
        {
            double liczba1;
                for (int i=0;i<strlen(liczba);i++)
                {
                    if (!isdigit(liczba[i])) //czy char jest liczba
                    {
                        break;
                    }
                }
            liczba1=atof(liczba); // zamiana char na double

            if(liczba1!=0) // przypisanie liczby do odpowiedniej pozycji
            { 
                if(kolumna==0){
                    dane[wiersz].X =liczba1;
                    kolumna+=1;
                } else if (kolumna==1){
                    dane[wiersz].Y =liczba1;
                    kolumna+=1;
                }else{
                    dane[wiersz].RHO =liczba1;
                    wiersz+=1;
                    kolumna=0;
                }

            }
            for(int i=0;i<10;i++)//zapis do programu
                liczba[i]='\0';// czyszczenie ciagu znakow
            n=0;
         if(znak==EOF)
        {
            break;
        }
        }else
        {
            if(znak!=32)
            {
                liczba[n]=znak;
                n++;
            }
        }
    }

    double sre[3];
    for(int i=0;i<3;i++)
    {
        sre[i]=srednia(dane,i,wiersz);
       // printf("srednia=%f\n",sre[i]);
    }

    sortowanie(dane,wiersz);

    double med[3];
    for(int i=0;i<3;i++)
    {
        med[i]=mediana(dane, i,wiersz);
        //printf("mediana=%f\n",med[i]);
    }

    double odch[3];
    for(int i=0;i<3;i++)
    {
        odch[i]=odchylenie(dane, i, wiersz, sre);
       // printf("odchylenie=%f\n",odch[i]);
    }

    fprintf(plik,"\n51.    %.5f    %.5f    %.3f", sre[0],sre[1],sre[2]);
    fprintf(plik,"\n52.    %.5f    %.5f    %.3f", med[0],med[1],med[2]);
    fprintf(plik,"\n53.    %.5f    %.5f    %.3f", odch[0],odch[1],odch[2]);

    fclose(plik);

    return 0;
}
