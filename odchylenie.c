#include "funkcje.h"

double odchylenie(struct Dane p_dane[],int p_i,int p_wiersz,double p_srednia[])
{
    double kwadrat=0;
    if(p_i==0){
        for(int i=0;i<p_wiersz;i++)
        kwadrat+=pow(p_dane[i].X-p_srednia[p_i],2);
        kwadrat/=p_wiersz;
        kwadrat=sqrt(kwadrat);
    }else if(p_i==1){
        for(int i=0;i<p_wiersz;i++)
        kwadrat+=pow(p_dane[i].Y-p_srednia[p_i],2);
        kwadrat/=p_wiersz;
        kwadrat=sqrt(kwadrat);
    }else{
        for(int i=0;i<p_wiersz;i++)
        kwadrat+=pow(p_dane[i].RHO-p_srednia[p_i],2);
        kwadrat/=p_wiersz;
        kwadrat=sqrt(kwadrat);
    }
    return kwadrat;
}