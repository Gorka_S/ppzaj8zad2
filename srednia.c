#include "funkcje.h"

double srednia(struct Dane p_dane[],int p_i,int p_wiersz)
{
    double sre=0;
        for(int i=0;i<p_wiersz;i++)
        {
            if (p_i==0){
                sre+=p_dane[i].X;
            }else if (p_i==1){
                sre+=p_dane[i].Y;
            }else{
                sre+=p_dane[i].RHO;
            }
        }
    sre/=p_wiersz;
    return sre;
}
