#include "funkcje.h"

double mediana(struct Dane p_dane[],int p_i,int p_wiersz)
{
    double med=0;
    if(p_i==0){
        if(p_wiersz%2==0) med=(p_dane[p_wiersz/2-1].X+p_dane[p_wiersz/2].X)/2;
        else med=p_dane[(p_wiersz)/2].X;
    }else if(p_i==1){
        if(p_wiersz%2==0) med=(p_dane[p_wiersz/2-1].Y+p_dane[p_wiersz/2].Y)/2;
        else med=p_dane[(p_wiersz)/2].Y;
    }else{
        if(p_wiersz%2==0) med=(p_dane[p_wiersz/2-1].RHO+p_dane[p_wiersz/2].RHO)/2;
        else med=p_dane[(p_wiersz)/2].RHO;
    }
    return med;
}