#include "funkcje.h"

void sortowanie(struct Dane p_dane[],int p_wiersz)
{
     int j=0;
     double pom=0;
     for(int i=1; i<p_wiersz; i++)
     {
             //wstawienie elementu w odpowiednie miejsce
             pom = p_dane[i].X; //ten element będzie wstawiony w odpowiednie miejsce
             j = i-1;

             //przesuwanie elementów większych od pom
             while(j>=0 && p_dane[j].X>pom)
             {
                    p_dane[j+1].X = p_dane[j].X; //przesuwanie elementów
                    --j;
             }
             p_dane[j+1].X = pom; //wstawienie pom w odpowiednie miejsce
     }
     for(int i=1; i<p_wiersz; i++)
     {
             //wstawienie elementu w odpowiednie miejsce
             pom = p_dane[i].Y; //ten element będzie wstawiony w odpowiednie miejsce
             j = i-1;

             //przesuwanie elementów większych od pom
             while(j>=0 && p_dane[j].Y>pom)
             {
                    p_dane[j+1].Y = p_dane[j].Y; //przesuwanie elementów
                    --j;
             }
             p_dane[j+1].Y = pom; //wstawienie pom w odpowiednie miejsce
     }
     for(int i=1; i<p_wiersz; i++)
     {
             //wstawienie elementu w odpowiednie miejsce
             pom = p_dane[i].RHO; //ten element będzie wstawiony w odpowiednie miejsce
             j = i-1;

             //przesuwanie elementów większych od pom
             while(j>=0 && p_dane[j].RHO>pom)
             {
                    p_dane[j+1].RHO = p_dane[j].RHO; //przesuwanie elementów
                    --j;
             }
             p_dane[j+1].RHO = pom; //wstawienie pom w odpowiednie miejsce
     }
    return;
}
